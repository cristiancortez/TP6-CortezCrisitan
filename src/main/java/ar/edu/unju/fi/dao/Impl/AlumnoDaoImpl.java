package ar.edu.unju.fi.dao.Impl;

import ar.edu.unju.fi.hibernate.base.HibernateBase;
import ar.edu.unju.fi.modelo.Alumno;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.dao.AlumnoDao;

public class AlumnoDaoImpl extends HibernateBase implements AlumnoDao{

	@Override	
	public List<Alumno> list() {
		Criteria criteria = getSession().createCriteria(Alumno.class);
		criteria.addOrder(Order.asc ("nombre"));
	
		return criteria.list();
	}
	@Override	
	public Alumno getAlumno(int legajo) {
		getSession().beginTransaction();
	
		return (Alumno)getSession().get(Alumno.class,legajo);
	}
	
	//Metodo implementado para la busqueda de alumno
	@Override	
	public List<Alumno> listByNombreyDni(String nombre,int dni ){
		Criteria criteria = getSession().createCriteria(Alumno.class);
		if (dni != 0) {
			
			criteria.add(Restrictions.eq("dni",dni));
		}
			if (!nombre.isEmpty()) {

				criteria.add(Restrictions.ilike("nombre",nombre,MatchMode.ANYWHERE));

			}
	
		return criteria.list();

	}
	
	
	
	
	
	//----------------------------------
	


	@Override
	public void insertar(Alumno alumno) {
		// TODO Auto-generated method stub
		  try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().save(alumno);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	      }
		
	}
	@Override
	public void update(Alumno alumno) {
		// TODO Auto-generated method stub
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().update(alumno);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	      }		
	}
		
	
	
	@Override
	public void delete(Alumno alumno) {
		// TODO Auto-generated method stub
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().delete(alumno);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	      }	
		
	}

	@Override
	public Alumno alumno(int legajo) {
		// TODO Auto-generated method stub
		return null;
	}
	


	
	
	

}
