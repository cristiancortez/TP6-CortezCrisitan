package ar.edu.unju.fi.dao;

import java.util.List;

import ar.edu.unju.fi.modelo.Usuario;

public interface UsuarioDao {

	Usuario validarUsuario(String usuario1, String clave);

	List<Usuario> list();

	List<Usuario> listByNombreByDni(String nombreUsuario1, int dni1);

	void insert(Usuario usuario);

	void update(Usuario usuario);

	void delete(Usuario usuario);


	
	
}
