package ar.edu.unju.fi.dao;

import java.util.List;

import ar.edu.unju.fi.modelo.Alumno;

public interface AlumnoDao {
	
	List<Alumno> list();
	
	List<Alumno> listByNombreyDni(String nombre, int dni);

	void insertar(Alumno alumno);

	void update(Alumno alumno);
	
	void delete (Alumno alumno);

	Alumno getAlumno(int legajo);


	Alumno alumno(int legajo);


}
