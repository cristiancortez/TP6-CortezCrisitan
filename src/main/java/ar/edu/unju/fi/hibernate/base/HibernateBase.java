

package ar.edu.unju.fi.hibernate.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Igual para cualquier proyecto
 * @author José Zapana
 */
public class HibernateBase {

    private SessionFactory sessionFactory;
    private Session session;

    public HibernateBase()
    {
      sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
      session = sessionFactory.openSession();

    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
