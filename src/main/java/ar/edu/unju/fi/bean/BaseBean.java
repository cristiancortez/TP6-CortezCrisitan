package ar.edu.unju.fi.bean;

import ar.edu.unju.fi.dao.AlumnoDao;
import ar.edu.unju.fi.dao.Impl.AlumnoDaoImpl;
import ar.edu.unju.fi.hibernate.base.HibernateBase;

public class BaseBean extends HibernateBase {
	private AlumnoDao alumnoDao;
	

	public AlumnoDao getAlumnoDao() {
		if (alumnoDao ==null)
			alumnoDao= new AlumnoDaoImpl();
		
		
		return alumnoDao;
	}

	public void setAlumnoDao(AlumnoDao alumnoDao) {
		this.alumnoDao = alumnoDao;
	}
	
	
	
	

}
