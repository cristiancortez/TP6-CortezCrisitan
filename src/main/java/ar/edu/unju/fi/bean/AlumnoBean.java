package ar.edu.unju.fi.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ar.edu.unju.fi.dao.AlumnoDao;
import ar.edu.unju.fi.dao.Impl.AlumnoDaoImpl;

import ar.edu.unju.fi.modelo.Alumno;
@ManagedBean (name="alumnoBean")
@SessionScoped
public class AlumnoBean extends BaseBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Alumno alumno;
	private List<Alumno> alumnos;
	
	private int legajo;
	private int dni;
	private String nombre;
	private String apellido;
	//-----------------------------
	private String event;
	
	
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public Alumno getAlumno() {
		return alumno;
	}
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	public List<Alumno> getAlumnos() {
		return alumnos;
	}
	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	public int getLegajo() {
		return legajo;
	}
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNombre2() {
		return nombre2;
	}
	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}
	public Integer getDni2() {
		return dni2;
	}
	public void setDni2(Integer dni2) {
		this.dni2 = dni2;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private Date fechaNacimiento;
	private String domicilio;
	private String email;
	private String telefono;
	
//para la busqueda
	private String nombre2;
	private Integer dni2;

	


//------------------------------------------------------	
	public void buscarAlumno(){
	//AlumnoDao alumnoDao = new AlumnoDaoImpl();
		//System.out.println("HOLAAA");
	
	alumnos = getAlumnoDao().listByNombreyDni(nombre, dni);
// System.out.println("el alumno fue encontrado "+ nombre2 + dni2 + getAlumno());
	
	
		
	}

//---------------------------------------------------------------
public void modificar(){	
	event = "modificar";
}
//-----------------------------------------------------------
public void nuevo(){
	alumno = new Alumno();
	event = "nuevo";
}
//------------------------------------------
public String guardar(){
	AlumnoDao alumnoDao = new AlumnoDaoImpl();
    alumno.setAlumno(alumno);

//	alumno.setLegajo(legajo);
//	alumno.setDni(dni);
//	alumno.setNombre(nombre);
//	alumno.setApellido(apellido);
//	alumno.setFechaNacimiento(fechaNacimiento);
//	alumno.setDomicilio(domicilio);
//	alumno.setEmail(email);
//	alumno.setTelefono(telefono);
	
	
	if (event.equals("modificar")){
		alumnoDao.update(alumno);
	}else{
		
		alumnoDao.insertar(alumno);
	}	
	buscarAlumno();
	RequestContext context = RequestContext.getCurrentInstance();
    context.execute("PF('alumno').hide();");
    context.update("form");
    context.update("form:alumnos");
	return "gestionAlumno";//
}

//--------------------------------------------------------------
public String eliminar(){
System.out.println("ELIMINAR");	
System.out.println(alumno.getNombre());
//	ClienteDAO clienteDAO = new ClienteDAOImpl();
	getAlumnoDao().delete(alumno);
	buscarAlumno();
	RequestContext context = RequestContext.getCurrentInstance();
    context.update("form");
    context.update("form:alumnos");// 
	return "gestionAlumno";//
}

//------------------------------------------------------------------
public String cerrar(){
	buscarAlumno();
	RequestContext context = RequestContext.getCurrentInstance();
    context.execute("PF('alumno').hide();");//
    context.update("form");
    context.update("form:alumnos");//
	return "gestionAlumno";
}	

//-----------------------------------------------------------

public String gestionAlumno(){
	return "gestionAlumno";
}	

}	
	
	
	
	
	
	
	
	
	
	
	


